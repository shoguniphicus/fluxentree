(function () {
'use strict';

	var url = require('url');
	var parsed = url.parse(location.href);

	var exportObject = {};

	switch(parsed.hostname){
		case "generic.dev":
			exportObject.image_url = "/leo/expo/img";
		break;

		default:
			exportObject.image_url = "img";
		break;
	}

	module.exports = {
	  IMAGE_URL:exportObject.image_url
	};

}());