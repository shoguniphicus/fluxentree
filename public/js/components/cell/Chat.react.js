(function () {
'use strict';


  var React = require('react');
  var ExpoAction = require('../../actions/ExpoAction');
  var WebConstants = require('../../constants/WebConstants');

  var ChatCell = React.createClass({
    render: function() {
      var setting = this.props.message;
      return (
        /*jshint ignore:start */
        <li>
          {setting.message}
        </li>
        /*jshint ignore:end */
      );
    },

  });

  module.exports = ChatCell;

}());