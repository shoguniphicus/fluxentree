(function () {
'use strict';

	var React = require('react');
	var $ = require('jquery');
	var jQuery = $;

	var ChatApp = require('./components/Chat.react');

	/*jshint ignore:start */
	React.render(<ChatApp />, $('#main').get(0));
	/*jshint ignore:end */

}());