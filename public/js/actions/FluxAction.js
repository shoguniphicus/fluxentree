(function () {
'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');

// Define action methods
var FluxActions = {

  // Events
  receiveMessage: function(data) {
    AppDispatcher.handleAction({
      actionType: Constants.RECEIVE_MESSAGE,
      data: data
    });
  },

  sendMessage: function(data){
    AppDispatcher.handleAction({
      actionType: Constants.SEND_MESSAGE,
      data: data
    });
  }
};

module.exports = FluxActions;

}());