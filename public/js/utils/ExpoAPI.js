(function () {
'use strict';

	var FluxAction = require('../actions/FluxAction');
	var $ = require('jquery');
	var jQuery = $;

	module.exports = {

	  // Load mock product data from localStorage into ProductStore via Action
	  getEventData: function(callback) {
	    // var data = JSON.parse(localStorage.getItem('events'));
	    
	    var jqxhr = $.ajax("./data/event.json")
		  .done(function(data) {
		  	FluxAction.receiveEvents(data);
		  	callback();
		  })
		  .fail(function(event) {
		    console.log( "error", event );
		  })
		  .always(function() {
		  });
	  },

	  getPeopleData: function(callback) {
	  	var jqxhr = $.ajax("./data/people.json")
		  .done(function(data) {
		  	FluxAction.receivePeople(data);
		  	callback();
		  })
		  .fail(function(event) {
		    console.log( "error", event );
		  })
		  .always(function() {
		  });
	  },

	  getTimelineData: function(callback){
	    var jqxhr = $.ajax("./data/timeline.json")
		  .done(function(data) {
		  	FluxAction.receiveTimeline(data);
		  	callback();
		  })
		  .fail(function(event) {
		    console.log( "error", event );
		  })
		  .always(function() {
		  });
	  }
	};

}());